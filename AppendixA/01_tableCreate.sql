DROP TABLE IF EXISTS heroes_powers;
DROP TABLE IF EXISTS power;
DROP TABLE IF EXISTS assistant;
DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero (
	hero_id serial PRIMARY KEY,
	hero_name varchar(50),
	hero_alias varchar(50),
	hero_origin text
);

CREATE TABLE assistant (
	assistant_id serial PRIMARY KEY,
	assistant_name varchar(50)
);


CREATE TABLE power(
	power_id serial PRIMARY KEY,
	power_name varchar(50),
	power_desc text
);

