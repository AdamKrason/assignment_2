INSERT INTO power (power_name, power_desc) VALUES ('Flight', 'Can fly');
INSERT INTO power (power_name, power_desc) VALUES ('teleportation', 'Can teleport');
INSERT INTO power (power_name, power_desc) VALUES ('Super strength', 'Very strong');
INSERT INTO power (power_name, power_desc) VALUES ('Fast', 'Is fast');

INSERT INTO heroes_powers (hero_id, power_id) VALUES (1, 1);
INSERT INTO heroes_powers (hero_id, power_id) VALUES (1, 2);
INSERT INTO heroes_powers (hero_id, power_id) VALUES (2, 3);
INSERT INTO heroes_powers (hero_id, power_id) VALUES (3, 3);