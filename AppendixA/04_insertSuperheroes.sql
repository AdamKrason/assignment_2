INSERT INTO superhero (hero_name, hero_alias, hero_origin) VALUES ('Khoi', 'Badboy', 'Does not look both ways when crossing the road');
INSERT INTO superhero (hero_name, hero_alias, hero_origin) VALUES ('Adam', 'Goodboy', 'Tells Khoi to look both ways before crossing the road');
INSERT INTO superhero (hero_name, hero_alias, hero_origin) VALUES ('Bruce Wayne', 'Batman', 'Vengeance');