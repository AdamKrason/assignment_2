DROP TABLE IF EXISTS heroes_powers;

CREATE TABLE heroes_powers (
	hero_id int REFERENCES superhero,
	power_id int REFERENCES power,
	PRIMARY KEY (hero_id, power_id)
);