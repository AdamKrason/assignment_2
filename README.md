# Assignment 2

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Assignment 2 is a Spring Boot application which uses plain Java, created through Spring Initializr in IntelliJ (Appendix B).
In addition to this, the assignment consists of SQL scripts which create a database different tables, relationships and data (Appendix A).

The Spring Boot application accesses data from a provided Chinook database and is separate from Appendix A.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)
- [Notes](#notes)

## Install


Required:
```
• Intellij Ultimate
• Java 17
• Postgres SQL driver dependency
• Postgres and PgAdmin
```

## Usage

```
• Clone GitLab repository
• Open repository folder in IntelliJ
• Create Chinook database in PgAdmin
• Make sure application.properties variables are correct according to your settings in PgAdmin
```

## Notes
```
• Run the application by executing the main file called "RunApplication".
• The aforementioned file shall not be edited, as the "Run"-method in "CustomerRunner" is the method which is executed upon execution of "main" in "RunApplication".
```

## Maintainers

[@AdamKrason](https://gitlab.com/AdamKrason)

[@eliohk](https://gitlab.com/eliohk)

## Contributing
Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.
## License

MIT © 2023 Adam & Khoi
