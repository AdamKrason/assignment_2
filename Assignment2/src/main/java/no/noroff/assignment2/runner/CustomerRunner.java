package no.noroff.assignment2.runner;

import no.noroff.assignment2.models.Customer;
import no.noroff.assignment2.repositories.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * <h1>CustomerRunner</h1>
 * A class which contains a CommandLineRunner which is a simple Spring Boot interface with a run method.
 * The run-method within this class will be called after the application context has been loaded.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-25
 */
@Component
public class CustomerRunner implements CommandLineRunner {
    final CustomerRepository customerRepo;

    /**
     * A mage-object is created with only a name, however the super-constructor initializes
     * the remaining necessary fields which heroes need.
     *
     * @param customerRepo The repository which contain data about customers
     */
    public CustomerRunner(CustomerRepository customerRepo) {
        this.customerRepo = customerRepo;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("-----------------------------------------------------------");
        System.out.println("Get list of all customers");
        System.out.println(customerRepo.getAll());
        System.out.println("-----------------------------------------------------------");
        System.out.println("Get customer by ID");
        System.out.println(customerRepo.getByID(12));
        System.out.println("-----------------------------------------------------------");
        System.out.println("Get customer by name");
        System.out.println(customerRepo.getCustomerByName("Helena"));
        System.out.println("-----------------------------------------------------------");
        System.out.println("Get a page of customers");
        System.out.println(customerRepo.getCustomerPage(5, 5));
        System.out.println("-----------------------------------------------------------");
        System.out.println("Adding new customer");
        System.out.println(customerRepo.create(new Customer(0, "Adam", "Tøffesen", "Norway",
                "1825", "112", "hei@gmail")));
        System.out.println("-----------------------------------------------------------");
        System.out.println("Updating existing customer");
        Customer customer = new Customer(61, "Adam", "Tøffesen", "Norway",
                "1825", "112", "hei@gmail");
        System.out.println(customerRepo.update(customer));
        System.out.println("-----------------------------------------------------------");
        System.out.println("Get country with most customers");
        System.out.println(customerRepo.getCountryWithMostCustomers());
        System.out.println("-----------------------------------------------------------");
        System.out.println("Get highest spending customer");
        System.out.println(customerRepo.getHighestSpender());
        System.out.println("-----------------------------------------------------------");
        System.out.println("Get most popular genre for a customer");
        System.out.println(customerRepo.getMostPopularGenre("Roberto"));
        System.out.println("-----------------------------------------------------------");
        System.out.println("Finito");
    }
}
