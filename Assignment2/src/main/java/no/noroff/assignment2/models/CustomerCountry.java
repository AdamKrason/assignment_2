package no.noroff.assignment2.models;

/**
 * <h1>CustomerCountry</h1>
 * A customerCountry-record to be used for holding data from the provided 'Chinook'-database
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-25
 */
public record CustomerCountry(String maxNum, int count) {
}
