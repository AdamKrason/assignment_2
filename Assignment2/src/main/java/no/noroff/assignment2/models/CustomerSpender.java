package no.noroff.assignment2.models;

/**
 * <h1>CustomerSpender</h1>
 * A customerSpender-record to be used for holding data from the provided 'Chinook'-database
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-25
 */
public record CustomerSpender(int id, String fName, String lName, double total) {
}
