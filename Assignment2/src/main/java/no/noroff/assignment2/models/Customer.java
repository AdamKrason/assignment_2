package no.noroff.assignment2.models;

/**
 * <h1>Customer</h1>
 * A customer-record to be used for holding data from the provided 'Chinook'-database
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-25
 */
public record Customer(int id, String fName, String lName, String country,
                       String pCode, String phone, String email) {
}
