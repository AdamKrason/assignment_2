package no.noroff.assignment2.repositories;

import no.noroff.assignment2.models.Customer;
import no.noroff.assignment2.models.CustomerCountry;
import no.noroff.assignment2.models.CustomerGenre;
import no.noroff.assignment2.models.CustomerSpender;

import java.util.List;

/**
 * <h1>CustomerRepository</h1>
 * A interface containing generic operations on a Customer-object.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-25
 */
public interface CustomerRepository extends CrudRepository<Customer, Integer>{

    /**
     * Executes a query to collect customers and return a page (list) constrained by the arguments (limit and offset)
     *
     * @param limit The maximum value of customers in a page
     * @param offset The value from where to start the page
     * @return List<Customer> A list of customer-objects which corresponds to the limit and offset variables.
     */
    public List<Customer> getCustomerPage(int limit, int offset);

    /**
     * Executes a query to find a specific customer by name in customer-table
     *
     * @param name The name of the customer-object
     * @return Customer A customer-object which corresponds to the name given as a parameter
     */
    public Customer getCustomerByName(String name);

    /**
     * Executes a query to collect the country with most unique customers in the customer-table.
     *
     * @return CustomerCountry A object which contains the country with most customers, and the amount of unique customers
     */
    public CustomerCountry getCountryWithMostCustomers();

    /**
     * Executes a query to collect the highest spender in the database
     *
     * @return CustomerSpender A object which contains information about the customer and the total of how much they have spent
     */
    public CustomerSpender getHighestSpender();

    /**
     * Executes a query to return the most popular genre of a specific customer by both first name and last name
     *
     * @param name The name of the customer which we want to find the most popular genre for
     * @return CustomerGenre A object which contain information about a customer and their most popular genre
     */
    public List<CustomerGenre> getMostPopularGenre(String name);
}
