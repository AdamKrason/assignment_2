package no.noroff.assignment2.repositories;

import java.util.List;

/**
 * <h1>CrudRepository</h1>
 * A interface containing generic CRUD (Create, Read, Update, Delete) operations on a repository for a specific type.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-25
 */
public interface CrudRepository<Customer, Integer> {

    /**
     * Executes a query to find a specific customer by id in customer-table
     *
     * @param id The id of the customer-object
     * @return Customer A customer-object which corresponds to the id given as a parameter
     */
    Customer getByID(int id);

    /**
     * Queries the Chinook-database for all customers in customer-table.
     *
     * @return List<Customer> A list containing all Customer-objects
     */
    List<Customer> getAll();

    /**
     * Executes a query to insert a customer into the customer-table
     *
     * @param object The customer object to be inserted
     * @return int Amount of rows which were inserted (should be 1)
     */
    int create(Customer object);

    /**
     * Executes a query to update a existing customer in the customer-table
     *
     * @param object customer The customer-object to be updated
     * @return int Amount of rows which were updated (should be 1)
     */
    int update(Customer object);
    void delete(int id);
}
