package no.noroff.assignment2.repositories;

import no.noroff.assignment2.models.Customer;
import no.noroff.assignment2.models.CustomerCountry;
import no.noroff.assignment2.models.CustomerGenre;
import no.noroff.assignment2.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>CustomerRepositoryImpl</h1>
 * A class which contain the implementations of the CustomerRepository interface.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-25
 */
@Repository
public class CustomerRepositoryImpl implements CustomerRepository{
    private final String url;
    private final String username;
    private final String password;

    /**
     * An implementation of the CustomerRepository needs three variables (url, username, password)
     * to be able to connect and query the provided database for customer information.
     *
     * @param url The JDBC URL of the provided 'Chinook'-database
     * @param username The login username of the database
     * @param password The login password of the database
     */
    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password
    ) {
        this.url = url;
        this.username = username;
        this.password = password;
    }


    public List<Customer> getAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customers.add(new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")));
            }

        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customers;
    }

    @Override
    public List<Customer> getCustomerPage(int limit, int offset) {
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customers.add(new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")));
            }

        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customers;
    }

    @Override
    public Customer getByID(int id) {
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customer = new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
            }

        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customer;
    }


    @Override
    public Customer getCustomerByName(String name) {
        String sql = "SELECT * FROM customer WHERE first_name LIKE ? OR last_name LIKE ?";
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%"+name+"%");
            statement.setString(2, "%"+name+"%");
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customer = new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
            }

        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customer;
    }

    @Override
    public int create(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.fName());
            statement.setString(2, customer.lName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.pCode());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            result = statement.executeUpdate();

        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return result;
    }

    /**
     * Executes a query to collect the country with most unique customers in the customer-table.
     *
     * @return CustomerCountry A object which contains the country with most customers, and the amount of unique customers
     */
    @Override
    public CustomerCountry getCountryWithMostCustomers() {
        String sql = "SELECT MAX(country), COUNT(country) FROM customer";
        CustomerCountry country = null;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            result.next();
            country = new CustomerCountry(result.getString("max"),
                    result.getInt("count"));
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return country;
    }


    @Override
    public CustomerSpender getHighestSpender() {
        String sql = "SELECT c.first_name, c.last_name, i.customer_id, i.total FROM invoice AS i " +
                "INNER JOIN customer as c ON i.customer_id = c.customer_id " +
                "WHERE i.total = (SELECT MAX(total) FROM invoice) " +
                "GROUP BY c.first_name, c.last_name, i.customer_id, i.total";
        CustomerSpender customer = null;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            result.next();
            customer = new CustomerSpender(result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getDouble("total"));
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customer;
    }


    @Override
    public List<CustomerGenre> getMostPopularGenre(String name) {
        String sql = "SELECT c.customer_id, g.name, c.first_name, c.last_name, COUNT(g.genre_id) as genreTracks\n " +
                "FROM customer AS c\n " +
                "INNER JOIN invoice as i ON c.customer_id = i.customer_id\n " +
                "INNER JOIN invoice_line as il ON i.invoice_id = il.invoice_id\n " +
                "INNER JOIN track as t ON t.track_id = il.track_id\n " +
                "INNER JOIN genre as g ON g.genre_id = t.genre_id\n " +
                "WHERE first_name LIKE ? OR last_name LIKE ? " +
                "GROUP BY c.customer_id , g.name\n " +
                "ORDER BY genreTracks DESC\n " +
                "FETCH FIRST 1 ROWS WITH TIES ";
        List<CustomerGenre> customer = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%"+name+"%");
            statement.setString(2, "%"+name+"%");
            ResultSet result = statement.executeQuery();
            while(result.next()){
                customer.add(new CustomerGenre(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("name"),
                        result.getInt("genreTracks")));
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customer;
    }


    public int update(Customer customer) {
        String sql = "UPDATE customer SET first_name = ?, " +
                "last_name = ?, " +
                "country = ?, " +
                "postal_code = ?, " +
                "phone = ?, " +
                "email = ? " +
                "WHERE customer_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.fName());
            statement.setString(2, customer.lName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.pCode());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.id());
            result = statement.executeUpdate();

        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public void delete(int id) {

    }
}
